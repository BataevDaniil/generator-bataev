'use strict';
const Generator = require('yeoman-generator');
const chalk = require('chalk');
const yosay = require('yosay');

const writeGulpfile = require('./write/gulpfile');
const writeMark = require('./write/mark');
const writeStyle = require('./write/style');
const writeScript = require('./write/script');
const writeSprite = require('./write/sprite');

const prompts = require('./prompts');

const config= require('./jsonGenerator/config');
const packageJsone = require('./jsonGenerator/packageJsone');

module.exports = class extends Generator {

	constructor(args, opts) {
		super(args, opts);
	}

	initializing() {
		let source = ['README.md',
		              '.gitignore',
		              '.babelrc',
		              '.editorconfig',
		              './tasks/browserSync.js',
		              './tasks/ngrok.js',
		              './tasks/clean.js',
		              './tasks/mode.js',
		              './tasks/log.js'];
		for (let item of source)
			this.fs.copy(
				this.templatePath(item),
				this.destinationPath(item)
			);
	}

	prompting() {
		return this.prompt(prompts).then(props => {
			this.props = props;
		});
	}

	writing() {
		if (!this.props.sprite)
			this.fs.copy(
				this.templatePath('./tasks/img.js'),
				this.destinationPath('./tasks/img.js'));

		if (this.props.jest)
			this.fs.copy(
				this.templatePath('./jest.config.js'),
				this.destinationPath('./jest.config.js'));

		config(this);
		packageJsone(this);
		writeGulpfile(this);
		writeMark(this);
		writeStyle(this);
		writeScript(this);
		writeSprite(this);
	}

	install() {
		this.installDependencies({
			npm: false,
			bower: false,
			yarn: true
		});
	}

	end() {
		this.log('All done');
	}
};
