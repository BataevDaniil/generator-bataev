const gulp = require('gulp');
const {reload}= require('browser-sync');

const imagemin = require('gulp-imagemin');
const spritesmith = require('gulp.spritesmith');
const merge = require('merge-stream');
const buffer = require('vinyl-buffer');
const gulpif = require('gulp-if');

const path = require('./config');
const mode = require('./mode');

gulp.task('sprite', () => {
	let spriteData = gulp.src(path.src.sprite)
		.pipe(spritesmith({
			imgName: path.sprite.imgName,
			cssName: path.sprite.cssName,
			cssFormat: path.sprite.cssFormat,
			algorithm: 'binary-tree',
			cssTemplate: 'stylus.template.mustache',
			cssVarMap: function(sprite) {
				sprite.name = 's-' + sprite.name
			}
		}));

	let imgStream = spriteData.img
		.pipe(gulpif('production' === mode, buffer()))
		.pipe(gulpif('production' === mode, imagemin({
			optimizationLevel: 3,
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			interlaced: true
		})))
		.pipe(gulp.dest(path.build.img))
		.pipe(reload({stream: true}));

	let sassStream = spriteData.css
		.pipe(gulp.dest(path.sprite.varible))
		.pipe(reload({stream: true}));

	return merge(imgStream, sassStream);
});