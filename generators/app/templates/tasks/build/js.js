const gulp = require('gulp');
const {reload}= require('browser-sync');

const path = require('../config');

gulp.task('js-build', function() {
	gulp.src(path.src.js)
		.pipe(gulp.dest(path.build.js))
		.pipe(reload({stream: true}));
});
