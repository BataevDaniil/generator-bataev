const gulp = require('gulp');
const {reload}= require('browser-sync');

const path = require('../config');

gulp.task('html-build', function() {
	gulp.src(path.src.html)
		.pipe(gulp.dest(path.build.html))
		.pipe(reload({stream: true}));
});
