const gulp = require('gulp');
const {reload}= require('browser-sync');

const autoprefixer = require('gulp-autoprefixer');
const removeComments = require('gulp-strip-css-comments');

const gulpif = require('gulp-if');

const path = require('../config');
const mode = require('../mode');

gulp.task('css-build', function() {
	return gulp.src(path.src.css)
		.pipe(autoprefixer({
			browsers: ['last 5 versions'],
			cascade: true
		}))
		.pipe(gulpif('production' === mode, removeComments()))
		.pipe(gulp.dest(path.build.css))
		.pipe(reload({stream: true}));
});
