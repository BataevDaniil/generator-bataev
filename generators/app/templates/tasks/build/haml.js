const gulp = require('gulp');
const {reload}= require('browser-sync');

const haml = require('gulp-ruby-haml');

const notify = require('gulp-notify');
const plumber = require('gulp-plumber');

const rename = require('gulp-rename');

const path = require('../config');


gulp.task('haml-build', function(){
	return gulp.src(path.src.haml)
		.pipe(plumber({
			errorHandler: notify.onError(function(err){
				return{
					title: 'Error pug',
					message: err.message
				}})
		}))
		.pipe(haml())
		.pipe(rename('index.html'))
		.pipe(gulp.dest(path.build.html))
		.pipe(reload({stream: true}));
});