const webpack = require('webpack');
const mode = require('./mode');

let config = {
	mode,
	output: {
		publicPath: '/js/'
	},
	watch: true,
	// devtool: 'source-map',
	devtool: 'eval',
	watchOptions: {
		aggregateTimeout: 100,
		poll: 100
	},
	resolve: {
		modules: ['./node_modules'],
		extensions: ['.tsx', '.ts', '.js', 'jsx', '.json']
	},
	resolveLoader: {
		modules: ['./node_modules'],
		moduleExtensions: ['-loader'],
		extensions: ['.js']
	},
	module: {
		rules: [{<% if (langScript == 0) { %>
			// regex ts or tsx or js or jsx
			test: /\.[tj]sx?$/i,
			exclude:/(node_modules|bower_components)/,
			use: [
				'babel',
				'ts'
			]<% } %><% if (langScript == 1) { %>,
			// regex js or jsx
			test: /\.jsx?$/i,
			exclude:/(node_modules|bower_components)/,
			use: [
				'babel'
			]<% } %>
		}]
	},
	plugins: [
	]
};

module.exports = config;