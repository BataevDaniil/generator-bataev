const gulp = require('gulp');

const path = require('./tasks/config');

const requireDir = require('require-dir');
requireDir('./tasks', { recurse: true });

gulp.task('build',
	gulp.series('clean',
	gulp.parallel(<% if (langMark== 1) { %>
	              'html-build',<% } %><% if (langScript == 1) { %>
	              'js-build',<% } %><% if (langMark == 0) { %>
	              'pug-build',<% } %><% if (langMark == 3) { %>
	              'haml-build',<% } %><% if (langStyle == 1) { %>
	              'css-build',<% } %><% if (langStyle == 3) { %>
	              'scss-build',<% } %><% if (langStyle == 0) { %>
	              'sass-build',<% } %><% if (langScript == 0) { %>
	              'ts-build'<% } %>)));

gulp.task('watch', () => {<% if (langMark == 0) { %>
	gulp.watch(path.watch.pug, gulp.series('pug-build'));<% } %><% if (langMark == 3) { %>
	gulp.watch(path.watch.haml, gulp.series('haml-build'));<% } %><% if (langStyle== 3) { %>
	gulp.watch(path.watch.scss, gulp.series('scss-build'));<% } %><% if (langStyle== 0) { %>
	gulp.watch(path.watch.sass, gulp.series('sass-build'));<% } %><% if (langMark == 1) { %>
	gulp.watch(path.watch.html, gulp.series('html-build'));<% } %><% if (sprite) { %>
	gulp.watch(path.watch.img, gulp.series('img-background'));
	gulp.watch(path.watch.sprite, gulp.series('sprite'));<% } else {%>
	gulp.watch(path.watch.img, gulp.series('img'));<% } %><% if (langScript == 1) { %>
	gulp.watch(path.watch.js, gulp.series('js-build'));<% } %><% if (langStyle == 1) { %>
	gulp.watch(path.watch.css, gulp.series('css-build'));<% } %>
});

gulp.task('default',
	gulp.series('build',
	gulp.parallel(<% if (sprite) { %>
	              'img-background',
	              'sprite',<% } else { %>
	              'img',<% } %>
	              'browser-sync',
	              'watch')));

