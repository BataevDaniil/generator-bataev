module.exports = [ {
	type: 'list',
	name: 'langMark',
	message: 'Choose what you will user in own project.',
	choices: [{
			name: 'pug',
			value: 0
		}, {
			name: 'haml',
			value: 3
		}, {
			name: 'html',
			value: 1
		}, {
			name: 'none',
			value: 2
		}],
	}, {
	type: 'list',
	name: 'langScript',
	message: 'Choose what you will user in own project.',
	choices: [{
			name: 'typescript',
			value: 0
		}, {
			name: 'javascript',
			value: 1
		}, {
			name: 'none',
			value: 2
		}],
	}, {
	type: 'confirm',
	name: 'jest',
	message: 'Use test Jest?',
	default: true
	}, {
	type: 'list',
	name: 'langStyle',
	message: 'Choose what you will user in own project.',
	choices: [{
			name: 'scss',
			value: 3
		}, {
			name: 'sass',
			value: 0
		}, {
			name: 'css',
			value: 1
		}, {
			name: 'none',
			value: 2
		}],
	}, {
	type: 'confirm',
	name: 'sprite',
	message: 'Create sprite for image?',
	default: true
	},
];
