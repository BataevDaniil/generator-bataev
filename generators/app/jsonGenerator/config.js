module.exports = function (data) {
	const configJs = {
		src: {},
		build: {'img': 'build/img'},
		watch: {},
		clean: 'build',
		browserSync: {
			port: 3000,
			baseDir: 'build'
		}
	};
	//src
	if (data.props.langMark == 1)
		configJs.src['html'] = 'src/index.html';

	if (data.props.langMark == 0)
		configJs.src['pug'] = 'src/pug/main.pug';

	if (data.props.langMark == 3)
		configJs.src['haml'] = 'src/haml/main.haml';

	if (data.props.langScript == 1)
		configJs.src['js'] = 'src/js/main.{js,jsx}';

	if (data.props.langScript == 0)
		configJs.src['ts'] = 'src/ts/**/*.{ts,tsx}';

	if (data.props.langStyle == 1)
			configJs.src['css'] = 'src/css/**/*.css';

	if (data.props.langStyle == 0)
		configJs.src['sass'] = 'src/sass/main.sass';

	if (data.props.langStyle == 3)
		configJs.src['scss'] = 'src/scss/main.scss';

	if (data.props.sprite) {
		configJs.src['img'] = 'src/img/background/**/*.*';
		configJs.src['sprite'] = 'src/img/sprite/**/*.*';
	} else
		configJs.src['img'] = 'src/img/**/*.*';

	//build
	if (data.props.langMark != 2)
		configJs.build['html'] = 'build';

	if (data.props.langScript != 2)
		configJs.build['js'] = 'build/js';

	if (data.props.langStyle != 2)
		configJs.build['css'] = 'build/css';

	//sprite
	if (data.props.sprite) {
		if (data.props.langStyle == 0)
			configJs['sprite'] = {
				varible: 'src/sass/varible',
				cssName: '_sprite.sass',
				cssFormat: 'sass'
			};
		if (data.props.langStyle == 3)
			configJs['sprite'] = {
				varible: 'src/scss/varible',
				cssName: '_sprite.scss',
				cssFormat: 'scss'

			};
		configJs.sprite['imgName'] = 'sprite.png';
	}

	//watch
	if (data.props.langMark == 1)
		configJs.watch['html'] = 'src/index.html';

	if (data.props.langMark == 0)
		configJs.watch['pug'] = 'src/pug/**/*.pug';

	if (data.props.langMark == 3)
		configJs.watch['haml'] = 'src/haml/**/*.haml';

	if (data.props.langStyle == 1)
		configJs.watch['css'] = 'src/css/**/*.css';

	if (data.props.langStyle == 0)
		configJs.watch['sass'] = 'src/sass/**/*.sass';

	if (data.props.langStyle == 3)
		configJs.watch['scss'] = 'src/scss/**/*.{scss,css}';

	if (data.props.sprite) {
		configJs.watch['img'] = 'src/img/background/**/*.*';
		configJs.watch['sprite'] = 'src/img/sprite/**/*.*';
	} else
		configJs.watch['sprite'] = 'src/img/**/*.*';

	data.fs.writeJSON('tasks/config.json', configJs);
}
