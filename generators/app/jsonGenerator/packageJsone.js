const _s = require('underscore.string');
let config = require('../templates/package')

module.exports = function (data) {
	config.name = _s.slugify(data.appname);
	if (data.props.jest) {
		config.scripts.['test'] = 'jest --config jest.config.json';
		config.dependencies['jest'] = '^22.4.2';
	}

	if (data.props.langScript !== 2 ) {
		if (data.props.langScript === 0) {
			config.dependencies['typescript'] = '2.7.2';
			config.dependencies['gulp-typescript'] = '4.0.1';
			config.dependencies['ts-loader'] = '4.0.0';
			if (data.props.jest)
				config.dependencies['ts-jest'] = '^22.4.1';
		}
		config.dependencies['babel-core'] = '^6.26.0';
		config.dependencies['babel-loader'] = '^7.1.3';
		config.dependencies['babel-preset-env'] = '^1.6.1';
		config.dependencies['babel-cli'] = '^6.26.0';
		config.dependencies['webpack-cli'] = '^2.0.9';
		config.dependencies['webpack'] = '4.0.0';
		config.dependencies['webpack-stream'] = '4.0.2';
	}

	if (data.props.langStyle !== 2) {
		if (data.props.langStyle === 0 || data.props.langStyle === 3) {
			config.dependencies['gulp-sass'] = '^3.1.0';
		}
		config.dependencies['gulp-autoprefixer'] = '^4.1.0';
		config.dependencies['gulp-strip-css-comments'] = '^2.0.0';
	}

	if (data.props.langMark === 0)
		config.dependencies['gulp-pug'] = '^3.3.0';

	if (data.props.langMark === 3)
		config.dependencies['gulp-ruby-haml'] = '^0.0.9';

	if (data.props.sprite) {
		config.dependencies['gulp.spritesmith'] = '^6.9.0';
		config.dependencies['vinyl-buffer'] = '^1.0.1';
		config.dependencies['merge-stream'] = '^1.0.1';
	}

	data.fs.writeJSON('./package.json', config);
}