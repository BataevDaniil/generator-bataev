module.exports = function (data) {
	if (data.props.langScript !== 2)
		data.fs.copyTpl(
			data.templatePath('./tasks/webpack.config.js'),
			data.destinationPath('./tasks/webpack.config.js'),{
				langScript: data.props.langScript
			});
	switch (data.props.langScript) {
		case 0: {
			data.fs.copyTpl(
				data.templatePath('main.ts'),
				data.destinationPath('src/ts/main.ts'));
			data.fs.copyTpl(
				data.templatePath('tsconfig.json'),
				data.destinationPath('tsconfig.json'));
			data.fs.copyTpl(
				data.templatePath('./tasks/build/ts.js'),
				data.destinationPath('./tasks/build/ts.js'));
			break;
		}
		case 1: {
			data.fs.copyTpl(
				data.templatePath('main.js'),
				data.destinationPath('src/js/main.js'));
			data.fs.copyTpl(
				data.templatePath('./tasks/build/js.js'),
				data.destinationPath('./tasks/build/js.js'));
			break;
		}
	}
};
