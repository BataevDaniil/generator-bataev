module.exports = function (data) {
	switch (data.props.langMark){
		case 0: {
			data.fs.copyTpl(
				data.templatePath('pug'),
				data.destinationPath('src/pug/'),
				{
					title: data.appname,
				});
			data.fs.copyTpl(
				data.templatePath('./tasks/build/pug.js'),
				data.destinationPath('./tasks/build/pug.js')
			);
			break;
		}
		case 1: {
			data.fs.copyTpl(
				data.templatePath('index.html'),
				data.destinationPath('src/index.html'),
				{
					title: data.appname,
				});
			data.fs.copyTpl(
				data.templatePath('./tasks/build/html.js'),
				data.destinationPath('./tasks/build/html.js')
			);
			break;
		}
		case 3: {
			data.fs.copyTpl(
				data.templatePath('haml'),
				data.destinationPath('src/haml/'),
				{
					title: data.appname,
				});
			data.fs.copyTpl(
				data.templatePath('./tasks/build/haml.js'),
				data.destinationPath('./tasks/build/haml.js')
			);
			break;
		}
	}
}