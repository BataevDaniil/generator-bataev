const mkdirp = require('mkdirp');

module.exports = function (data) {
	if (data.props.sprite) {
		mkdirp.sync('src/img/sprite');
		mkdirp.sync('src/img/background');
		data.fs.copy(
			data.templatePath('./tasks/sprite.js'),
			data.destinationPath('./tasks/sprite.js'));
		data.fs.copy(
			data.templatePath('./tasks/imgBackground.js'),
			data.destinationPath('./tasks/imgBackground.js'));
		data.fs.copyTpl(
			data.templatePath('stylus.template.mustache'),
			data.destinationPath('stylus.template.mustache'),{
				langStyle: data.props.langStyle
			});

		if (data.props.langStyle === 0) {
			data.fs.write('src/sass/varible/_sprite.sass', '');
			data.fs.copy(
				data.templatePath('_sprite.sass'),
				data.destinationPath('src/sass/mixin/_sprite.sass'))
		}
		else if (data.props.langStyle === 3) {
			data.fs.write('src/scss/varible/_sprite.scss', '');
			data.fs.copy(
				data.templatePath('_sprite.scss'),
				data.destinationPath('src/scss/mixin/_sprite.scss'))
		}
	}
	else
		mkdirp.sync('src/img');
}

