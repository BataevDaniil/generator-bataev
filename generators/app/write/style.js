module.exports = function (data) {
	switch (data.props.langStyle) {
		case 3: {
			data.fs.copyTpl(
				data.templatePath('scss'),
				data.destinationPath('src/scss'),{
					sprite: data.props.sprite,
				});
			data.fs.copyTpl(
				data.templatePath('./tasks/build/sass.js'),
				data.destinationPath('./tasks/build/scss.js'), {
				langStyle: data.props.langStyle,
			});
			break;
		}
		case 0: {
			data.fs.copyTpl(
				data.templatePath('sass'),
				data.destinationPath('src/sass'),{
					sprite: data.props.sprite,
				});
			data.fs.copyTpl(
				data.templatePath('./tasks/build/sass.js'),
				data.destinationPath('./tasks/build/sass.js'), {
				langStyle: data.props.langStyle,
			});
			break;
		}
		case 1: {
			data.fs.copyTpl(
				data.templatePath('main.css'),
				data.destinationPath('src/css/main.css'),{
					sprite: data.props.sprite,
				});
			data.fs.copy(
				data.templatePath('./tasks/build/css.js'),
				data.destinationPath('./tasks/build/css.js'));
			break;
		}
	}
}