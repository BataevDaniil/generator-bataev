module.exports = function (data) {
	data.fs.copyTpl(
		data.templatePath('gulpfile.js'),
		data.destinationPath('gulpfile.js'),
		{
			langMark: data.props.langMark,
			langScript: data.props.langScript,
			langStyle: data.props.langStyle,
			sprite: data.props.sprite,
		});
}