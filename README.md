# generator-bataev

## Installation

```bash
npm install -g yo
npm install -g generator-lucifer
```

Then generate your new project:

```bash
yo lucifer
```

## Start
if development
```bash
yarn development
```
else if production
```bash
yarn production
```
